extends Node2D
var current_game
var scn_flag
var sub_scn_flag
var demo_scn_flag
var message_number = -1 #Current Message
export var message_max_number = 3 #The max chosen messages for this specific script.
export var message_speed = 10 #Font scroll speed.
var message = "SAMPLE."
var charname = "null"
var previous_charname = "null"
var msg_scale = Vector2(1,0.5)
var msg_scale_default = Vector2(1,0.5)
var play_clip = 0
var wait_timer = 1
var scene_finished = 0
var answer = "null"
var soda_sold_out = 0
var sfx_play = 0
var debug_timer = 1.00

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	message_number = 0
	current_game = $"/root/Globals".current_game
	scn_flag = $"/root/Globals".scn_flag
	sub_scn_flag = $"/root/Globals".sub_scn_flag
	demo_scn_flag = $"/root/Globals".demo_scn_flag
	
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if $"/root/Globals".debug_mode == 1:
		if debug_timer > 0.00:
			debug_timer -=4*delta
		if debug_timer < 0.01:
			print("Message Number == ",message_number)
			debug_timer = 1.00
		
	if not charname == previous_charname:
		msg_scale = Vector2(1,0.1)
		previous_charname = charname 
	
	if msg_scale.y < msg_scale_default.y:
		msg_scale.y+=0.025
	
#	if msg_scale.x < msg_scale_default.x:
#		msg_scale.x+=0.05
		pass
	current_game = $"/root/Globals".current_game
	scn_flag = $"/root/Globals".scn_flag
	sub_scn_flag = $"/root/Globals".sub_scn_flag
	demo_scn_flag = $"/root/Globals".demo_scn_flag
	
	
	#Global Messages
	if $"/root/Globals".locked == 1:
		#Debug Room Dialogue
		message_max_number = 0
		if message_number == 0:
			charname = "Megaman"
			message = "It's locked..."
			return
	
	if $"/root/Globals".soda == 1 and $"/root/Globals".question == 0:
		#Vending dialogue 1
		message_max_number = 2
		if message_number == 0:
			charname = "Megaman"
			message = "It's a vending machine."
			return
		
		if message_number == 1 and $"/root/Globals".question == 0:
			charname = "Megaman"
			message = "Do you want to put 100 zenny in?"
			
		if message_number == 2 and $"/root/Globals".question == 0:
			charname = "Megaman"
			message = "Do you want to put 100 zenny in?"
			$"/root/Globals".question = 1
			return
		
		
	
	if $"/root/Globals".soda == 3 and answer == "yes":
		#Drink action
		message_max_number = 2
		if message_number == 0:
			charname = "Megaman"
			message = "You got a soft drink."
			if not $"/root/menu_sfx/soda_get_sfx".playing and sfx_play == 0:
				$"/root/menu_sfx/soda_get_sfx".play()
				$"/root/menu_sfx/glug_sfx".stop()
				sfx_play = 1
			
			$"/root/Globals".question = 0
			return
		
		if message_number == 1:
			charname = "Megaman"
			message = "glug-glug" 
			$"/root/Globals".megaman_animation = "anim_048"
			if not $"/root/menu_sfx/glug_sfx".playing and sfx_play == 0 and message == "glug-glug":
				$"/root/menu_sfx/glug_sfx".play()
				sfx_play = 1
			return
		
		if message_number == 2:
			charname = "Megaman"
			message = "You feel refreshed!"
			$"/root/Globals".soda = 5
			$"/root/Globals".wait = 2
			return
		
	
	
	if $"/root/Globals".soda == 3 and answer == "no":
		#Drink action
		message_max_number = -1
		if message_number == 0:
			charname = "Megaman"
			message = "You got a soft drink."
			$"/root/Globals".soda = -1
			$"/root/Globals".question = 0
			return


	if $"/root/Globals".soda == -1:
		#Drink action
		$"/root/Globals".soda = 0
		$"/root/Globals".question = 0
		answer = "null"
		message_max_number = -1
		if message_number == 0:
			charname = "Megaman"
			message = "SAMPLE"
		
	
	
	if $"/root/Globals".near_npc == 1:
		#Debug Room Dialogue
		if scn_flag == -1 and $"/root/Globals".npc_name == "Roll" and $"/root/Globals".area == "Cardon Debug" and $"/root/Globals".next_message == 0:
			message_max_number = 2
			if message_number == 0:
				charname = "Roll"
				message = "Hey Megaman! Welcome to the debug room."
			if message_number == 1:
				message = "We're currently working on the story data now."
			return
			
		#Demo Ruin Story
	if demo_scn_flag == 0 and $"/root/Globals".area == "Demo Ruin B1" and scene_finished == 0 and $"/root/Globals".next_message == 0:
		message_max_number = 3
		if message_number == 0:
			$"/root/Globals".msg_box_y = 50
			charname = "Roll"
			message = "Are you sure about this Megaman?"
			if not $"R1-3".playing and play_clip == 0:
				$"R1-3".play()
				play_clip = 1
			
		if message_number == 1:
			if not $"MM1-1".playing and play_clip == 0:
				$"R1-3".stop()
				$"MM1-1".play()
				play_clip = 1
			$"/root/Globals".msg_box_y = $"/root/Globals".msg_box_y_default
			charname = "Megaman"
			message = "Yeah, no one's ever been here before right? Don't tell me you believe those ghost stories?"
		
		if message_number == 2:
			if not $"R2-3-4-Mix".playing and play_clip == 0:
				$"R2-3-4-Mix".play()
				play_clip = 1
			$"/root/Globals".msg_box_y = 50
			charname = "Roll"
			message = "Well, the townspeople did seem really worried about it... Be careful okay?"
			
		if message_number == 3:
			if not $"MM2-4".playing and play_clip == 0:
				$"R1-3".stop()
				$"MM2-4".play()
				play_clip = 1
			$"/root/Globals".msg_box_y = $"/root/Globals".msg_box_y_default
			charname = "Megaman"
			message = "Don't worry about it Roll, we'll be fine."
		
		
		
	pass

func _on_MM11_finished():
	$"/root/Globals".next_message = 0 #Broken Currently.
	pass # Replace with function body.


func _on_R13_finished():
	$"/root/Globals".next_message = 0
	$"R1-3".stop()
	pass # Replace with function body.


func _on_R234Mix_finished():
	$"/root/Globals".next_message = 0
	pass # Replace with function body.


func _on_MM24_finished():
	$"/root/Globals".next_message = 0
	pass # Replace with function body.
