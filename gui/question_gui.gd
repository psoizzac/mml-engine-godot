extends Node2D
var selection
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	selection = "yes"
	$"/root/Globals".wait = 1
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("move_forward"):
		if selection == "no":
			$"/root/menu_sfx/move_sfx".play()
			selection = "yes"
		print(selection)

	if Input.is_action_just_pressed("move_back"):
		if selection == "yes":
			$"/root/menu_sfx/move_sfx".play()
			selection = "no"
		print(selection)

	if selection == "no":
		$outline/yes.visible = false
		$outline/no.visible = true
	
	if selection == "yes":
		$outline/yes.visible = true
		$outline/no.visible = false
		
	if Input.is_action_just_pressed("jump"):
		$"/root/menu_sfx/select_sfx".play()
		$"/root/Story_Data".answer = selection
		if $"/root/Globals".soda == 2:
			$"/root/Globals".megaman_animation = "anim_045"
			$"/root/Globals".soda = 3
		$"/root/Globals".question = 3
		queue_free()
