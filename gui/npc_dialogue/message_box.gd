extends Node2D
var timer = 10
var char_num = 0 #Max text scroll position, updates depending on message size
var char_current = 0 #Current message scroll position
var char_timer = 1
var skip_timer = 1
var default_scale = Vector2(1,0.5)
var msg_timer = 1
var charname = "null"
var previous_charname = "null"
var message_wait = 1

var message_number = 0 #Current Message
export var message_max_number = 3 #The max chosen messages for this specific script.
export var message_speed = 12 #Font scroll speed.
var message = "This is a test of the message box system, have a nice day and remember to re-initialize daily."

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$"/root/Story_Data".sfx_play = 0
	$"/root/Globals".wait = 1
	timer = 10
	char_current = 0.1
	$TextureRect/Dialogue.set_text(message)
	$TextureRect.set_scale(Vector2(1,0.01))
	$"/root/Story_Data".scene_finished = 0
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if Input.is_action_just_pressed("jump") and char_current >= char_num:
		$"/root/Story_Data".sfx_play = 0
		pass
	
	if $"/root/Globals".soda == 1:
		if message_number == 2:
			$"/root/Globals".question = 1
			$"/root/Globals".soda = 2
			close_box()
#	print("Message Number2 = ",message_number)
	
	if Input.is_action_just_pressed("jump") and char_current >= char_num:
			char_current = 0.00
			if msg_timer > 0.01:
				msg_timer-=1*delta
				close_box()
			if msg_timer < 0.02:
				msg_timer = 1
				return
	
	if not $TextureRect.scale.y == $"/root/Story_Data".msg_scale.y:
		$TextureRect.scale = $"/root/Story_Data".msg_scale
	set_position(Vector2($"/root/Globals".msg_box_x,$"/root/Globals".msg_box_y))
	
	message_max_number = $"/root/Story_Data".message_max_number
	$"/root/Story_Data".message_number = message_number
	
	if Input.is_action_pressed("special_weapon") and char_current < char_num:
		char_current = char_num-1
	
	if Input.is_action_pressed("special_weapon"):
		skip_timer -=5*delta
	
	if skip_timer < 0.01 and char_current >= char_num and $"/root/Globals".soda == 0:
		message_number+=1
		skip_timer = 1
		char_current = 0
	
	if char_current < char_num:
		char_timer-=10*delta
		if char_timer < 0.01:
			$msg_sfx.play()
			char_timer = 1
		pass
	if message_number > message_max_number:
		$"/root/Story_Data".scene_finished = 1
		$"/root/Globals".wait = 0
		$"/root/Story_Data".message = " "
		if $"/root/Globals".soda > 0:
			$"/root/Story_Data".message = "Do you want to put 100 zenny in?"
			char_current = char_num
		$"/root/Story_Data".message_number = 0
		if $"/root/Globals".soda == 5:
			$"/root/Globals".zenny -=100
			$"/root/Globals".soda = -1
		queue_free()
	
	if Input.is_key_pressed(KEY_Y):
		char_current = 0
	char_num = $TextureRect/Dialogue.get_total_character_count()

	timer+=message_speed
	if timer > 10:
		timer = 0
		if char_current < char_num+1:
			char_current+=1
			pass
		$TextureRect/Dialogue.visible_characters = char_current
		if char_current >= char_num:
			$TextureRect.set_speed_scale(0.5)
		else:
			$TextureRect.set_speed_scale(0)
			$TextureRect.set_frame(0)
		
	#Dialogue Tree's go here.
	if message_number == 0:
		message = $"/root/Story_Data".message
		if Input.is_action_just_pressed("jump") and char_current >= char_num and not $"/root/Globals".wait == 0:
			message_number +=1
			$"/root/Story_Data".play_clip = 0
			char_current = 0.00
		$TextureRect/Dialogue.set_text(message)

	if message_number == 1:
		message = $"/root/Story_Data".message
		if Input.is_action_just_pressed("jump") and char_current >= char_num and not $"/root/Globals".wait == 0:
			message_number +=1
			$"/root/Story_Data".play_clip = 0
			char_current = 0.00
		$TextureRect/Dialogue.set_text(message)
	
	if message_number == 2:
		message = $"/root/Story_Data".message
		if Input.is_action_just_pressed("jump") and char_current >= char_num and not $"/root/Globals".wait == 0:
			char_current = 0.00
			$"/root/Story_Data".play_clip = 0
			message_number +=1
		$TextureRect/Dialogue.set_text(message)

	
	if message_number == 3:
		message = $"/root/Story_Data".message
		if Input.is_action_just_pressed("jump") and char_current >= char_num and not $"/root/Globals".wait == 0:
			char_current = 0.00
			message_number +=1
			$"/root/Story_Data".play_clip = 0
		$TextureRect/Dialogue.set_text(message)
	
	if message_number == 4:
		message = $"/root/Story_Data".message
		if Input.is_action_just_pressed("jump") and char_current >= char_num and not $"/root/Globals".wait == 0:
			char_current = 0.00
			message_number +=1
			$"/root/Story_Data".play_clip = 0
		$TextureRect/Dialogue.set_text(message)

	
	if message_number == 5:
		message = $"/root/Story_Data".message
		if Input.is_action_just_pressed("jump") and char_current >= char_num and not $"/root/Globals".wait == 0:
			char_current = 0.00
			message_number +=1
			$"/root/Story_Data".play_clip = 0
		$TextureRect/Dialogue.set_text(message)

	if message_number == 6:
		message = $"/root/Story_Data".message
		if Input.is_action_just_pressed("jump") and char_current >= char_num and not $"/root/Globals".wait == 0:
			char_current = 0.00
			message_number +=1
			$"/root/Story_Data".play_clip = 0
		$TextureRect/Dialogue.set_text(message)


	
	if message_number == 7:
		message = $"/root/Story_Data".message
		if Input.is_action_just_pressed("jump") and char_current >= char_num and not $"/root/Globals".wait == 0:
			char_current = 0.00
			message_number +=1
			$"/root/Story_Data".play_clip = 0
		$TextureRect/Dialogue.set_text(message)


	if message_number == 8:
		message = $"/root/Story_Data".message
		if Input.is_action_just_pressed("jump") and char_current >= char_num and not $"/root/Globals".wait == 0:
			char_current = 0.00
			message_number +=1
			$"/root/Story_Data".play_clip = 0
		$TextureRect/Dialogue.set_text(message)

		
	if message_number == 9:
		message = $"/root/Story_Data".message
		if Input.is_action_just_pressed("jump") and char_current >= char_num and not $"/root/Globals".wait == 0:
			char_current = 0.00
			message_number +=1
			$"/root/Story_Data".play_clip = 0
		$TextureRect/Dialogue.set_text(message)
	
	if $"/root/Globals".next_message == 1 and char_current >= char_num:
		message_wait -=1*delta
		if message_wait < 0.01:
			$"/root/Globals".next_message = 0
			message_wait = 1
			char_current = 0.00
			close_box()
			
	pass

func close_box():
	$"/root/Story_Data".play_clip = 0
	print("Box Closed")
	message_number +=1
	if $"/root/Story_Data".msg_scale.y > 0.01:
		$"/root/Story_Data".msg_scale.y-=0.01
		pass
