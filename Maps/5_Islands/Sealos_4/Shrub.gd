extends MeshInstance
var destroyed = preload("res://Models/small_explosion.tscn")
export var timer = float(4)
export var hp = 4
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass









func explode():
	if $"/root/Globals".debug_mode == 1:
		print("Object Location = ",global_transform)
	var b = destroyed.instance()
	b.start(self.global_transform)
	get_tree().get_root().add_child(b)
	queue_free()


func _on_Tree_Box_area_entered(area):
	print("Hit!")
	hp-=1
	if hp < 1:
		explode()
	pass # Replace with function body.
