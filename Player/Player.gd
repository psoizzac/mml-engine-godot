extends KinematicBody
var DOWN = -1
var gravity_default = Vector3(0,DOWN,0)*12
var gravity = Vector3(0,DOWN,0)*12
var gravity_stop = Vector3(0,DOWN,0)*0
var accel = 0
export var speed = 55
export var default_speedrate = 10
export var speedrate = 10
export var jump_speed = 5
export var spin = 0.125
var velocity = Vector3(0,DOWN,0)
var jump = false
var jump_release = 0
var state = "jump"
var rot_x = 0
var rot_y = 0
var rot_z = 0
var floor_normal = Vector3(0,-1,0)
var slope_min_velocity = 0
var max_bounces = 1
var floor_max_angle = deg2rad(45)
var timer = 0
var up = 0
var down = 0
var left = 0
var right = 0
var rot_y2 = rot_y
var maxhp = 50
var hp = maxhp
var wpn_energy = 100
var max_wpn_energy = 100
var floor_timer = 10
var stuck_timer = 500
var model = 0
var bt_speed = 1
var bt_rot = 0
var hit_combo = 0
var hit_timer = 0
var inside_enemy = 0
var combo_timer = 0
var previous_velocity = Vector3(0,0,0)
var jet_skate = 0
var jet_skate_timer = 10
var Bullet = preload("res://buster_shot_3d.tscn")
var energy
var power
var rapid
var ranged
var buster_time
var previous_speed = speed
var wait = 0
var print_timer = 1
var shoot = 0
var quick_turn = 0
var quick_turn_timer = 1
var debug_timer = 1
var recovery_timer = 1


func _ready():
	$"/root/Global_Audio/sealos_theme".play()
	#Called when the scene starts.
	energy = $"/root/Globals".energy
	power = $"/root/Globals".power
	rapid = $"/root/Globals".rapid
	ranged = $"/root/Globals".ranged
	maxhp = $"/root/Globals".maxhp
	max_wpn_energy = $"/root/Globals".max_wpn_energy
	model = $Megaman
	hp = maxhp
	buster_time = energy
	previous_speed = speed
	if $"/root/Globals".jump_springs == 1:
		jump_speed = 8
		pass
	
	pass





func _fixed_process():
	energy = $"/root/Globals".energy
	power = $"/root/Globals".power
	rapid = $"/root/Globals".rapid
	ranged = $"/root/Globals".ranged
	maxhp = $"/root/Globals".maxhp
	max_wpn_energy = $"/root/Globals".max_wpn_energy
	if $"/root/Story_Data".soda == 3 and $"/root/Story_Data".message_number == 1:
		hp = $"/root/Globals".maxhp
		$"/root/Globals".current_hp = hp
		pass
	pass




func _process(delta):
	if state == "recover2":
		recovery_timer-=2*delta
		if recovery_timer < 0.01:
			recovery_timer = 1
			if $Megaman/AnimationPlayer.current_animation_position > 0.2 and $Megaman/AnimationPlayer.current_animation_position < 0.50:
				$Megaman/AnimationPlayer.play("anim_034")
				$Megaman/AnimationPlayer.seek(0.00)
			
			if $Megaman/AnimationPlayer.current_animation_position > 0.51 and $Megaman/AnimationPlayer.current_animation_position < 0.70:
				$Megaman/AnimationPlayer.play("anim_037")
				$Megaman/AnimationPlayer.seek(0.00)
	
	if debug_timer < 0.01:
		print("state == ",state)
		print($Megaman/AnimationPlayer.current_animation)
		print("anim pos == ",$Megaman/AnimationPlayer.current_animation_position)
		debug_timer = 1
	if debug_timer > 0.00:
		debug_timer -=5*delta
	
	
	if $"/root/Story_Data".message == "You feel refreshed!":
		hp = maxhp
	
	$"/root/Globals".current_hp = hp
	
	if wait > 0 and $"/root/Globals".soda == 3 and $"/root/Story_Data".answer == "yes":
		if not $Megaman/AnimationPlayer.current_animation == "anim_045":
			$Megaman/AnimationPlayer.play("anim_045")
			
		pass
	
	if quick_turn_timer >= 0.01 and quick_turn == 1:
		quick_turn_timer -=2.5*delta
	
	if quick_turn == 1 and quick_turn_timer < 0.01:
		$"/root/Globals".wait = 0
		quick_turn_timer = 1
		quick_turn = 0
		pass
	
	
	
	wait = $"/root/Globals".wait
	if shoot == 1 and $Megaman/AnimationPlayer.current_animation == "anim_059" and $Megaman/AnimationPlayer.current_animation_position > 0.2:
		$Megaman/AnimationPlayer.set_speed_scale($"/root/Globals".rapid*0.001) 
	
	
	if wait > 0:
		state = "stand"
		velocity.x = 0
		velocity.z = 0
		if not $Megaman/AnimationPlayer.current_animation == "anim_042":
			$Megaman/AnimationPlayer.play("anim_042")
			pass
	
	if print_timer > 0:
		print_timer-=2*delta
	if print_timer < 0.01:
#		print(wait)
#		print($"/root/Globals".area)
		print_timer = 1
	
	
	if state == "run":
		if not $mm_walk_sfx.current_animation == "anim_001":
			$mm_walk_sfx.play("anim_001")
	if not state == "run":
		if not $mm_walk_sfx.current_animation == "anim_000":
			$mm_walk_sfx.play("anim_000")
	#Skate Animation
#	print($Megaman/AnimationPlayer.current_animation)
	if state == "skate":
		if not $Megaman/AnimationPlayer.current_animation == "anim_049" and not $Megaman/AnimationPlayer.current_animation == "anim_050":
			$Megaman/AnimationPlayer.play("anim_049")
		pass
	
	
	if state == "skate":
		speed = previous_speed*2
	if not state == "skate":
		speed = previous_speed
	
	if not state == "jump":
		speedrate = default_speedrate
	if state == "jump":
		speedrate = 1
		
	if $Floor_Check.is_colliding() == true:
		jump_release = 0
		gravity = gravity_stop
		stuck_timer = 300
	if $Floor_Check.is_colliding() == false:
		gravity = gravity_default
	
	stuck_timer -=1
#	print(stuck_timer)
#	print($Floor_Check.is_colliding())
	if not is_on_ceiling() and state == "jump" and stuck_timer < 1:
		set_translation(Vector3(0,5,0))
	if not is_on_ceiling() and state == "knockback" and stuck_timer < -100:
		set_translation(Vector3(0,5,0))
		stuck_timer = 500
	if is_on_ceiling():
		stuck_timer = 300
	#AI Controls
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	if Input.is_action_just_pressed("Test_Key"):
		hp = 24
		hit_timer = 0.00
		hit_combo+=1
		combo_timer = 30
		state = "stand"
		state = "hurt"
		$Player_SFX/hit_snd.play()
		$Megaman/AnimationPlayer.seek(0,0)
		
		hit_timer = 25
		up = 0
		down = 0
		left = 0
		right = 0
		
	timer +=1
	if timer > 120:
		down = randi() % 2
		up = randi() % 2
		left = randi() % 2
		right = randi() % 2
		timer = 0
		pass
	pass
	





func _physics_process(delta):
	$"/root/Globals".player_x = transform.basis.x
	$"/root/Globals".player_y = transform.basis.y
	$"/root/Globals".player_z = transform.basis.z
	if not state == "hurt" and not state == "knockback" and not state == "knockback2" and not state == "recover"and not state == "recover2" and not wait > 0:
		get_input()
	
	if inside_enemy > 0:
		if hit_timer > 10:
			hit_timer = 10
	
#	print(is_on_ceiling())
#	print(state)
#	print(hp)
	
	if combo_timer > 0:
		combo_timer -=1
	if combo_timer < 1:
		combo_timer = 0
		hit_combo = 0
	
	#Combat Damage
	if inside_enemy == 1 and hit_timer < 1 and wait == 0:
		if hit_combo < 4:
			$Player_SFX/hit_snd.play()
			hit_combo +=1
			hit_timer = 20
			combo_timer = 45
			if hp > 0:
				state = "hurt"
			hp-=2
			print("Hit by enemy!")
	pass
	
	if state == "hurt":
		if hp < 1.00:
			previous_velocity = velocity
			velocity.y = 8.25
			hit_combo = 0
			state = "knockback"
			gravity = gravity_default
			$Player_SFX/knockdown_snd.play()
			
		$Megaman/AnimationPlayer.set_speed_scale(1)
		if velocity.y > 1 or velocity.y < -1:
			previous_velocity = velocity
			velocity.y = 7.25
			state = "knockback"
			$Player_SFX/knockdown_snd.play()
		if hit_combo >= 3:
			previous_velocity = velocity
			state = "knockback"
			$Player_SFX/knockdown_snd.play()
			velocity.y = 6.25
			hit_combo = 0
		
		velocity.x = 0
		velocity.z = 0
		
		
		hit_timer -=1
		if hit_timer < 1:
			hit_timer = 0
		if hit_timer == 0:
			state = "stand"
		
		if Input.is_action_pressed("move_forward") and not state == "skate" :
			if not $Megaman/AnimationPlayer.current_animation == "anim_029":
				$Megaman/AnimationPlayer.play("anim_029")
				pass
			velocity -= -transform.basis.z * accel
			accel+=speedrate/2
		
		if Input.is_action_pressed("move_back") and not state == "skate":
			velocity += -transform.basis.z * accel
			accel+=speedrate/2
			if not $Megaman/AnimationPlayer.current_animation == "anim_029":
				$Megaman/AnimationPlayer.play("anim_029")
				pass
		
		if Input.is_action_pressed("strafe_right"):
			velocity += -transform.basis.x * accel
			accel+=speedrate/2
			if not $Megaman/AnimationPlayer.current_animation == "anim_029" and not state == "skate":
				$Megaman/AnimationPlayer.play("anim_029")
				pass
		
		if Input.is_action_pressed("strafe_left"):
			velocity -= -transform.basis.x * accel
			accel+=speedrate/2
			if not $Megaman/AnimationPlayer.current_animation == "anim_029" and not state == "skate":
				$Megaman/AnimationPlayer.play("anim_029")
				pass
		
		if not Input.is_action_pressed("move_back") and not Input.is_action_pressed("strafe_left") and not Input.is_action_pressed("strafe_right") and not Input.is_action_pressed("move_forward") and not state == "skate":
			velocity -= -transform.basis.z * accel
			accel+=speedrate/2
			if not $Megaman/AnimationPlayer.current_animation == "anim_029":
				$Megaman/AnimationPlayer.play("anim_029")
				pass
		
		
		if accel > speed/2:
			accel = speed/2
		velocity += gravity * delta
		
	#Knockdown1
	if state == "knockback":
		
		if not $Megaman/AnimationPlayer.current_animation == "anim_031":
			$Megaman/AnimationPlayer.play("anim_031")
			$MM_BT/AnimationPlayer.play("anim_031")
		
		velocity.x = -previous_velocity.x*1.2
		velocity.z = -previous_velocity.z*1.2
		
		hit_timer -=1
		if hit_timer < 1:
			hit_timer = 0
		if hit_timer == 0 and is_on_ceiling():
			state = "recover"
			$Player_SFX/hit_ground_snd.play()
		
		if accel > speed/2:
			accel = speed/2
		velocity += gravity * delta


	if state == "recover":
		$Megaman/AnimationPlayer.set_speed_scale(1)
		if $Megaman/AnimationPlayer.current_animation == "anim_031" and $Megaman/AnimationPlayer.current_animation_position < 0.80:
			$Megaman/AnimationPlayer.play("anim_032")
		
		if $Megaman/AnimationPlayer.current_animation == "anim_031" and $Megaman/AnimationPlayer.current_animation_position >= 0.80:
			$Megaman/AnimationPlayer.play("anim_033")
			if $"/root/Globals".helmet == 0:
				hp-=4
		velocity.x = 0
		velocity.z = 0
		velocity.y = 0

	if state == "recover2":
		pass

	if state == "recover3":
		if not $Megaman/AnimationPlayer.current_animation == "anim_038":
			$Megaman/AnimationPlayer.play("anim_038")


#	velocity = move_and_slide_with_snap(velocity, Vector3(0,-1,0), Vector3(0,-1,0))

	if state == "run":
		if not $Megaman/AnimationPlayer.current_animation == "anim_001":
			$Megaman/AnimationPlayer.play("anim_001")
			$Megaman/AnimationPlayer.set_speed_scale(0.95)
	
	if state == "stand" and not Input.is_action_pressed("Turn_Left") and not Input.is_action_pressed("Turn_Right"):
		if not $Megaman/AnimationPlayer.current_animation == "anim_000" and shoot == 0 and wait == 0:
			$Megaman/AnimationPlayer.play("anim_000")
			$Megaman/AnimationPlayer.set_speed_scale(0.8)
		
		if shoot == 1:
			$Megaman/AnimationPlayer.play("anim_059")
			$Megaman/AnimationPlayer.set_speed_scale(1)
	
	

	
	rot_y2 = rot_y
	velocity = move_and_slide_with_snap(velocity,Vector3(0,-32,0),Vector3(0,-1,0))

	if not is_on_ceiling():
		velocity += gravity * delta

	if is_on_ceiling():
		if state == "jump":
			velocity.y = -1
			$Player_SFX/land_sfx.play()
			state = "stand"
			
	
	if not is_on_ceiling():
		if state == "walk" or state == "run" and velocity.y < -2:
			state = "jump"
	

	if jump == true and not state == "jump" and not state == "hurt" and not state == "knockback" and not state == "skate":
		velocity.y = jump_speed
		$Megaman/AnimationPlayer.play("anim_017")
		$Megaman/AnimationPlayer.set_speed_scale(0.8)
		$Player_SFX/jump_sfx.play()
		state = "jump"
	
	if accel > speed:
		accel = speed
	
	bt_speed = $Megaman/AnimationPlayer.get_playing_speed()
	if not $MM_BT/AnimationPlayer.current_animation == $Megaman/AnimationPlayer.current_animation:
		$MM_BT/AnimationPlayer.play($Megaman/AnimationPlayer.current_animation) 
		$MM_BT/AnimationPlayer.set_speed_scale(bt_speed)
		$MM_BT.set_rotation_degrees(Vector3(0,rot_y,0))


func get_input():
	var vy = velocity.y
	velocity = Vector3(0,0,0)
	if not Input.is_action_pressed("attack"):
		shoot = 0
	
	#Stutter Shot Glitch emulation
#	if Input.is_action_pressed("attack") and Input.is_action_just_pressed("move_forward") and not $Megaman/AnimationPlayer.current_animation == "anim_001" :
#		var b2 = Bullet.instance()
#		b2.start($Muzzle.global_transform)
#		get_parent().add_child(b2)
	
	if Input.is_action_pressed("attack") and wait == 0 and $"/root/Globals".energy > 0 and $"/root/Globals".rapid_timer < 0.02 and not state == "skate":
		shoot = 1
		var b = Bullet.instance()
		b.start($Muzzle.global_transform)
		get_parent().add_child(b)
		$Megaman/AnimationPlayer.play("anim_059")
		$Megaman/AnimationPlayer.seek(0.0)
		$MM_BT/AnimationPlayer.play("anim_059")
		$MM_BT/AnimationPlayer.seek(0.0)
	
	if Input.is_action_pressed("jet_skate_button") and not state == "skate" and state == "stand" and $"/root/Globals".has_jet_skates == 1:
#		print(jet_skate_timer)
		jet_skate_timer-=0.30
		if jet_skate_timer < 0.01:
			state = "skate"

		if not $Megaman/AnimationPlayer.current_animation == "anim_049":
			accel+=speedrate*1.5
	
	if not Input.is_action_pressed("jet_skate_button"):
		jet_skate_timer = 10
		if state == "skate":
			state = "stand"
		pass
	
	if Input.is_action_pressed("jet_skate_button") and not state == "hurt" and state == "skate" and $"/root/Globals".has_jet_skates == 1:
		
		
		if not $Megaman/AnimationPlayer.current_animation == "anim_049":
			velocity += -transform.basis.z * accel
			accel+=speedrate
		
		if not state == "jump":
			state = "skate"
			if rot_y > 180:
				rot_y-=10
			if rot_y < 180:
				rot_y +=10
			$Megaman.set_rotation_degrees(Vector3(0,rot_y,0))
	
	if Input.is_action_just_pressed("move_forward"):
		accel = 0.00
	if Input.is_action_just_pressed("move_back"):
		accel = 0.00
	if Input.is_action_just_pressed("strafe_left"):
		accel = 0.00
	if Input.is_action_just_pressed("strafe_right"):
		accel = 0.00
	
	if Input.is_action_pressed("move_forward") and not state == "hurt" and not state == "skate":
		velocity += -transform.basis.z * accel
		accel+=speedrate
		if not state == "jump":
			state = "run"
			if rot_y > 180:
				rot_y-=10
			if rot_y < 180:
				rot_y +=10
			$Megaman.set_rotation_degrees(Vector3(0,rot_y,0))




	if Input.is_action_pressed("move_back") and not state == "skate":
		velocity += transform.basis.z * accel
		accel+=speedrate
		if not state == "jump":
			state = "run"
			if rot_y > 269:
				rot_y +=10
			if rot_y < 181 and rot_y > 0:
				rot_y -=10
			if rot_y > 359:
				rot_y = 0
			$Megaman.set_rotation_degrees(Vector3(0,rot_y,0))
	
	if Input.is_action_pressed("strafe_right"):
		if not state == "skate":
			if not Input.is_action_pressed("move_forward") and not Input.is_action_pressed("move_back"):
				velocity -= -transform.basis.x * accel
			
			if Input.is_action_pressed("move_forward") or Input.is_action_pressed("move_back") or state == "skate":
				velocity -= -transform.basis.x * accel/2
			
		
		
		
		if state == "skate":
			velocity -= -transform.basis.x * accel/2
		
		accel+=speedrate
		if not state == "jump" and not state == "skate":
			state = "run"
			if not Input.is_action_pressed("move_forward") and not Input.is_action_pressed("move_back"):
				if rot_y > 359:
					rot_y = 0
				
				if rot_y > 90:
					rot_y-=10
				
				if rot_y < 90:
					rot_y+=10
			$Megaman.set_rotation_degrees(Vector3(0,rot_y,0))
	
	if Input.is_action_pressed("strafe_left"):
		
		if not state == "skate":
			if not Input.is_action_pressed("move_forward") and not Input.is_action_pressed("move_back"):
				velocity += -transform.basis.x * accel
			
			if Input.is_action_pressed("move_forward") or Input.is_action_pressed("move_back") or state == "skate":
				velocity += -transform.basis.x * accel/2
			
		
		
		
		if state == "skate":
			velocity += -transform.basis.x * accel/2
		
		accel+=speedrate
		if not state == "jump" and not state == "skate":
			state = "run"
			if not Input.is_action_pressed("move_forward") and not Input.is_action_pressed("move_back"):
				
				if rot_y > 359:
					rot_y = 0
				if rot_y < 270:
					rot_y +=10
				if rot_y < 1:
					rot_y -=20
				if rot_y < 89:
					rot_y = 270
				
			$Megaman.set_rotation_degrees(Vector3(0,rot_y,0))
	
	if not Input.is_action_pressed("move_forward")	and not Input.is_action_pressed("move_back") and not Input.is_action_pressed("strafe_left") and not Input.is_action_pressed("strafe_right"):
		if not state == "jump" and not state == "hurt" and not state == "knockback" and not state == "skate":
			accel = 0.00
			rot_y = 180
			$Megaman.set_rotation_degrees(Vector3(0,rot_y,0))
			state = "stand"
		
	if Input.is_action_pressed("Turn_Left"):
		rotate_y(spin/5)
		if state == "stand" and shoot == 0:
			if not $Megaman/AnimationPlayer.current_animation == "anim_010":
				$Megaman/AnimationPlayer.play("anim_010")
				$Megaman/AnimationPlayer.set_speed_scale(0.9)
				$MM_BT/AnimationPlayer.set_speed_scale(0.9)
		pass
	
	if Input.is_action_pressed("Turn_Right"):
		rotate_y(-spin/5)
		if state == "stand" and shoot == 0:
			if not $Megaman/AnimationPlayer.current_animation == "anim_010":
				$Megaman/AnimationPlayer.play_backwards("anim_010")
				$MM_BT/AnimationPlayer.play_backwards("anim_010")
				$Megaman/AnimationPlayer.set_speed_scale(0.9)
				$MM_BT/AnimationPlayer.set_speed_scale(0.9)
		pass
	
	if Input.is_action_pressed("move_back"):
		if Input.is_action_just_pressed("action_button")and $Floor_Check.is_colliding() == true:
			if quick_turn_timer > 0.01:
				$"/root/Globals".wait = 1
				quick_turn = 1
			rotate_y(deg2rad(180))
		pass
	
	if Input.is_action_pressed("strafe_left"):
		if Input.is_action_just_pressed("action_button") and $Floor_Check.is_colliding() == true:
			if quick_turn_timer > 0.01:
				$"/root/Globals".wait = 1
				quick_turn = 1
			rotate_y(deg2rad(90))
		pass
		
	if Input.is_action_pressed("strafe_right") and $Floor_Check.is_colliding() == true:
		if Input.is_action_just_pressed("action_button"):
			if quick_turn_timer > 0.01:
				$"/root/Globals".wait = 1
				quick_turn = 1
			rotate_y(deg2rad(-90))
		pass
	
		pass
	velocity.y = vy
	jump = false
	if Input.is_action_just_pressed("jump") and jump == false:
		jump = true
	
	if Input.is_action_just_released("jump") and not state == "hurt" and not state == "knockback" and jump_release == 0:
		if velocity.y > 0:
			velocity.y = -1
		jump_release = 1
		pass
		
		
	pass
	
	$MM_BT.set_rotation_degrees(Vector3(0,rot_y,0))
	
	if state == "knockback":
		if $Megaman/AnimationPlayer.current_animation == "anim_031" and $Megaman/AnimationPlayer.current_animation_position > 0.80:
			$Megaman/AnimationPlayer.set_speed_scale(0.00)
			$MM_BT/AnimationPlayer.set_speed_scale(0.00)
	
	if state == "jump":
		if $Megaman/AnimationPlayer.current_animation == "anim_018" and $Megaman/AnimationPlayer.current_animation_position > 0.50:
			$Megaman/AnimationPlayer.set_speed_scale(0.00)
			$MM_BT/AnimationPlayer.set_speed_scale(0.00)
			
	
	$"/root/Globals".rot_y = rot_y








#func _unhandled_input(event):
#	if event is InputEventMouseMotion:
#		rotate_y(-lerp(0, spin, event.relative.x/10))
#	pass


func _on_AnimationPlayer_animation_finished(anim_num):
	if $Megaman/AnimationPlayer.current_animation == "anim_045" and $"/root/Globals".soda == 2:
		$"/root/menu_sfx/soda_get_sfx".play()
		$"/root/Globals".soda = 3
		pass
	
	if state == "skate":
		$Megaman/AnimationPlayer.play("anim_050")
	
	
	if state == "jump":
		$Megaman/AnimationPlayer.play("anim_018")
		$MM_BT/AnimationPlayer.play("anim_018")
	
	if state == "hurt":
		state = "stand"
		accel = 0
		
	
	if state == "knockback":
		inside_enemy = 0
		$Megaman/AnimationPlayer.set_speed_scale(0.00)
		$MM_BT/AnimationPlayer.set_speed_scale(0.00)
#		if not $Megaman/AnimationPlayer.current_animation == "anim_031":
#			$Megaman/AnimationPlayer.play("anim_031")
#			$MM_BT/AnimationPlayer.play("anim_031")
			
			
	if state == "recover" and hp > 0:
		if $Megaman/AnimationPlayer.current_animation_position > 0.19:
			if hp > 0:
				state = "recover2"
			if rot_y == 0:
				rotate_y(deg2rad(180))
				rot_y = 180
			
			if rot_y == 90:
				rotate_y(deg2rad(270))
				rot_y = 180
			
			if rot_y == 270:
				rotate_y(deg2rad(90))
				rot_y = 180
	
	if state == "recover2":
		if $Megaman/AnimationPlayer.current_animation_position >= 0.60:
			state = "recover3"
		pass
			
			
	if state == "recover3":
		if $Megaman/AnimationPlayer.current_animation_position > 0.69:
			$Megaman/AnimationPlayer.play("anim_038")
			inside_enemy = 0
	

func _unhandled_input(event):
	pass
	
func _on_Enemy_Check_area_shape_entered(area_id, area, area_shape, self_shape):
	inside_enemy = 1

func _on_Enemy_Check_area_shape_exited(area_id, area, area_shape, self_shape):
	inside_enemy = 0

func _on_Car_Check_area_entered(area):
	if accel < 3:
		accel = 55
		velocity += -transform.basis.z * accel
	inside_enemy = 1
	hit_combo = 99
	hit_timer = 25
	combo_timer = 45
	if hp > 0:
		state = "hurt"
		hit_combo = 99
	if $"/root/Globals".joke == 0:
		hp-=4
	print("Hit by car!")


func _on_NPC_Talk_Check_area_entered(area):
	
	pass # Replace with function body.


func _on_Car_Check_area_exited(area):
	if $"/root/Globals".joke == 1:
		$Player_SFX/item_fail_sfx.play()
		$Player_SFX/knockdown_snd.stop()
	inside_enemy = 0
	pass # Replace with function body.





func _on_Mus_Area_area_entered(area):
	$"/root/Globals".area = "Demo Ruin B1"
	$"/root/Global_Audio/sealos_theme".stop()
	$"/root/Global_Audio/kimo_theme".play()
	print("music area entered")
	pass # Replace with function body.


func _on_Mus_Area_area_exited(area):
#	get_tree().get_root().get_node("./Kimotoma_Theme_B1/Mus_Area").queue_free()
	pass # Replace with function body.
