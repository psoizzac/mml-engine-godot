extends Node2D
var timer = 10
var char_num = 0 #Max text scroll position, updates depending on message size
var char_current = 0 #Current message scroll position

var message_number = 0 #Current Message
export var message_max_number = 3 #The max chosen messages for this specific script.
export var message_speed = 10 #Font scroll speed.
export var message = "This is a test of the message box system, have a nice day and remember to re-initialize daily."

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	timer = 10
	char_current = 0.1
	$TextureRect/Dialogue.set_text(message)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if message_number > message_max_number:
		queue_free()
	
	if Input.is_key_pressed(KEY_Y):
		char_current = 0
	char_num = $TextureRect/Dialogue.get_total_character_count()

	timer+=message_speed
	if timer > 10:
		timer = 0
		if char_current < char_num+1:
			char_current+=1
			pass
		$TextureRect/Dialogue.visible_characters = char_current
		if char_current >= char_num:
			$TextureRect.set_speed_scale(0.5)
		else:
			$TextureRect.set_speed_scale(0)
			$TextureRect.set_frame(0)
		
	#Dialogue Tree's go here.
	if message_number == 0:
		message = "Welcome to the Cardon Forest debug room."
		if Input.is_action_just_pressed("jump") or Input.is_action_just_pressed("action_button") and char_current >= char_num:
			message_number +=1
			char_current = 0.00
		$TextureRect/Dialogue.set_text(message)

	if message_number == 1:
		message = "There's not a lot going on yet, but stay tuned for more information."
		if Input.is_action_just_pressed("jump") or Input.is_action_just_pressed("action_button") and char_current >= char_num:
			message_number +=1
			char_current = 0.00
		$TextureRect/Dialogue.set_text(message)
	
	if message_number == 2:
		message = "Who knows maybe something interesting will be hidden here in the future."
		if Input.is_action_just_pressed("jump") or Input.is_action_just_pressed("action_button") and char_current >= char_num:
			char_current = 0.00
			message_number +=1
		$TextureRect/Dialogue.set_text(message)

	
	if message_number == 3:
		message = "End Message Test"
		if Input.is_action_just_pressed("jump") or Input.is_action_just_pressed("action_button") and char_current >= char_num:
			char_current = 0.00
			message_number +=1
		$TextureRect/Dialogue.set_text(message)
	
	pass
