extends Spatial
var near_player = 0
var soda_stock = 0
var script_c = load("res://msg/message_box.tscn")
var inst

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	soda_stock = 1
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("action_button") and near_player == 1 and $"/root/Globals".wait == 0 and $"/root/Globals".zenny > 99:
		$"/root/Globals".soda = 1
		$"/root/Globals".wait = 1
	if Input.is_action_just_pressed("action_button") and near_player == 1 and $"/root/Globals".wait == 0 and $"/root/Globals".zenny < 100:
		$"/root/Globals".soda = -99
		$"/root/Globals".wait = 1
		pass


func _on_Soda_Area_area_entered(area):
	near_player = 1
	print("Near Vending Machine")
	pass # Replace with function body.


func _on_Soda_Area_area_exited(area):
	near_player = 0
	print("Left Vending Machine")
	pass # Replace with function body.
