extends Spatial
var open = 0
var near_player = 0
export var door_speed = 20
var timer = 0.25
var default_x
var sound_played = 0
export var locked = 0
var blue = 1
var red = 1
var green = 1
var alpha = 1
var switch_color = 0
export var switch = 0 #If set to 1 it Requires an area3D switch to open the door
var switch_button_active = 0
export var enemy_lock = 0 #If set to 1 you have to defeat all enemies in the room to unlock the door.
var defeated_enemies = 0 #Number of enemies the player has defeated.
export var required_enemies = 0 #How many enemies you need to defeat to disable the lock.
export var required_switches = 0
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	default_x = global_transform.origin.x
	open = 0
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	defeated_enemies = $"/root/Globals".defeated_enemies
	if enemy_lock > 0:
		if defeated_enemies >= required_enemies:
			locked = 0
			open = 1
			switch = 2
			enemy_lock = 0
		
		pass
		
	if locked == 1:
		if switch_color == 0:
			red -=0.25*delta
			blue-=0.25*delta
			green-=0.25*delta
			if red < 0.70 and green < 0.80 and blue < 0.70:
				switch_color = 1
		
		if switch_color == 1:
			red +=0.25*delta
			blue+=0.25*delta
			green+=0.25*delta
			if red > 0.99 and green > 0.99 and blue > 0.99:
				switch_color = 0
		pass
	
	if Input.is_action_just_pressed("action_button") and open == 0 and near_player == 1 and locked == 0 and enemy_lock == 0 and switch == 0:
		if sound_played == 0:
			$door_open_sfx.play()
		open = 1
		sound_played = 1

	if Input.is_action_just_pressed("action_button") and open == 0 and near_player == 1 and locked == 1 and switch_button_active == 1:
		if sound_played == 0:
			$door_open_sfx.play()
			locked = 0
			switch = 2
			open = 1
			sound_played = 1

	if Input.is_action_just_pressed("action_button") and open == 0 and near_player == 1 and locked == 1:
		
		pass
	
	if open == 1 and timer >= 0.01 and switch == 0:
		
		$DoorL.translate(Vector3(-door_speed*delta,0,0))
		$DoorR.translate(Vector3(-door_speed*delta,0,0))
		timer-=1*delta
		pass

	if open == 1 and timer >= 0.01 and switch == 2:
		
		$DoorL.translate(Vector3(-door_speed*delta,0,0))
		$DoorR.translate(Vector3(-door_speed*delta,0,0))
		timer-=1*delta
		pass

	if timer < 0.01 and near_player == 0 and switch == 0:
		open = 2
		timer = 0.25
	
	if open == 2:
		if sound_played == 0:
			$door_open_sfx.play()
			sound_played = 1
		$DoorL.translate(Vector3(+door_speed*delta,0,0))
		$DoorR.translate(Vector3(+door_speed*delta,0,0))
		timer-=1*delta
		if timer < 0.01:
			open = 0
			timer = 0.25
		pass
	
	pass


func _on_Area_area_entered(area):
	if enemy_lock == 1:
		$"/root/Globals".locked = 1
	
	if locked == 1:
		$"/root/Globals".locked = 1
	if open == 0:
		sound_played = 0
	print("Near the door.")
	near_player = 1
	pass # Replace with function body.


func _on_Area_area_exited(area):
	$"/root/Globals".locked = 0
	print("Left the door.")
	if sound_played == 1:
		sound_played = 0
		near_player = 0
	
#	if open == 2:
#		timer = 0.25
#		open = 0
	pass # Replace with function body.


func _on_Switch_Area_area_entered(area):
	print("Near the switch")
	switch_button_active = 1
	near_player = 1
	
	pass # Replace with function body.


func _on_Switch_Area_area_exited(area):
	print("Left the switch")
	switch_button_active = 0
	near_player = 0
	pass # Replace with function body.
