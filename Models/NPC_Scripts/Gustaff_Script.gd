extends KinematicBody

export var speed = 325
var accel = 0.00
var speedrate = 50
var direction = Vector3()
var gravity = -20
var velocity = Vector3()
var state = "null"
var rot_x = 0
var rot_y = 0
var rot_z = 0
var character = 0
var move_rot_y = 1
var facing = 0
var print_timer = 0
var left = 0
var right = 0
var up = 0
var down = 0
var jump = 0
var timer = 0
var wait = 0
export var AI_Mode = 0 #AI Mode: 0 = Roam, 1 = Patrol up/down 2 = Patrol left/right, 3 = Scan, 4 = Alert 5 = Chase.
export var Type = 0 #AI Colour
var AI_Wait = 0


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	$Reaverbot_Root/AnimationPlayer.play("anim_000")
	print(facing)
	pass




func _process(delta):
	#AI Controls 1
	print(AI_Wait)
	if AI_Wait > 0:
		AI_Wait -=5*delta
		if AI_Wait < 1:
			AI_Wait = 0
			
		
		#AI Controls 2
	if Input.is_key_pressed(KEY_T):
		wait = 1
		
	if Input.is_key_pressed(KEY_Y):
		state = "stand"
		up = 0
		down = 0
		left = 0
		right = 0
		wait = 0
	timer +=1
	if timer > 120 and AI_Wait < 1:
		down = randi() % 2
		up = randi() % 2
		left = randi() % 2
		right = randi() % 2
		timer = 0
		pass
	pass
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.

func _physics_process(delta):
	$Reaverbot_Root/Gustaff_Bottom/AnimationPlayer.play($Reaverbot_Root/AnimationPlayer.current_animation)
	
	if $Reaverbot_Root/Wall_Detection.is_colliding() and AI_Wait < 1:
		print(get_name()+" has bumped into a wall")
		if up == 1:
			up = 0
			down = 1
			AI_Wait = 5
			return
		
		if down == 1:
			up = 1
			down = 0
			AI_Wait = 5
			return
			
		if left == 1:
			left = 0
			right = 1
			AI_Wait = 5
			return
			
		if right == 1:
			left = 1
			right = 0
			AI_Wait = 5
			return
	
	if wait == 1:
		state = "stand"
		left = 0
		right = 0
		up = 0
		down = 0
	move_rot_y +=0.01
	if left == 1:
		right = 0
		up = 0
		down = 0

	if right == 1:
		left = 0
		up = 0
		down = 0


	if up == 1:
		left = 0
		right = 0
		down = 0


	if down == 1:
		left = 0
		right = 0
		up = 0

	if accel > speed:
		accel = speed


	#Controls and Animations
	direction = Vector3(0,0,0)

	if left == 1 and up == 0 and down == 0 and right == 0:
		direction.x -=1+accel
		accel +=speedrate
		if state != "jump":
			state = "walk"
		if rot_y > 359 and not down == 1 and not down == 1:
			rot_y = 0

		if rot_y < 270 and rot_y > 0 and not up == 1 and not down == 1:
			rot_y+=10

		if rot_y < 1 and not up == 1 and not down == 1:
			rot_y-=20
			if rot_y < -89:
				rot_y = 270
		$Reaverbot_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_001" and state == "walk":
			$Reaverbot_Root/AnimationPlayer.play("anim_001")

	if right == 1 and up == 0 and down == 0 and left == 0:
		direction.x +=1+accel
		accel +=speedrate
		if state != "jump":
			state = "walk"

		if rot_y > 359 and not up == 1 and not down == 1:
			rot_y = 0

		if rot_y > 90 and not up == 1 and not down == 1:
			 rot_y -=10

		if rot_y < 90 and not up == 1 and not down == 1:
			rot_y+=10

		$Reaverbot_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_001":
			$Reaverbot_Root/AnimationPlayer.play("anim_001")

	if up == 1 and left == 0 and right == 0 and down == 0:

		direction.z-=accel
		accel +=speedrate
		if state != "jump":
			state = "walk"
		if rot_y > 180:
			rot_y -=10
		if rot_y < 180:
			rot_y +=10
		$Reaverbot_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_001" and state == "walk":
			$Reaverbot_Root/AnimationPlayer.play("anim_001")

	if down == 1 and left == 0 and right == 0 and up == 0:
		direction.z +=1+accel
		accel +=speedrate
		if state != "jump":
			state = "walk"
			if rot_y > 269:
				rot_y +=10
			if rot_y < 181 and rot_y > 0:
				rot_y -=10
			if rot_y > 359:
				rot_y = 0
		$Reaverbot_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_001":
			$Reaverbot_Root/AnimationPlayer.play("anim_001")


	#Movement and gravity.
	direction = direction.normalized()
	direction = direction * accel * delta

	if velocity.y > 2:
		gravity = -30
	else:
		gravity = -20

	velocity.y += gravity * delta
	velocity.x = direction.x
	velocity.z = direction.z

	if state == "stand":
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_000":
			$Reaverbot_Root/AnimationPlayer.play("anim_000")
		up = 0
		down = 0
		left = 0
		right = 0


	velocity = move_and_slide(velocity,Vector3(0,1,0))

	#Collision detection
	if is_on_floor() and jump == 1:
		state = "jump"
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_005":
			$Reaverbot_Root/AnimationPlayer.play("anim_005")
		velocity.y = 9

	if velocity.y > 4 or velocity.y < -1 and $Reaverbot_Root/AnimationPlayer.current_animation == "anim_005":
			$Reaverbot_Root/AnimationPlayer.play("anim_006")
			state = "jump"
			pass

	if not is_on_floor() and velocity.y < -2:
		state = "jump"
		if state == "jump":
			velocity.y += gravity * delta
			pass

	if state == "jump":
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_006" and velocity.y >= 4 or velocity.y <= -4:
			$Reaverbot_Root/AnimationPlayer.play("anim_006")

	if is_on_floor():
		if state != "stand" and velocity.x == 0 and velocity.z == 0:
			state = "stand"

		if state == "jump":
			state = "stand"


func _on_AnimationPlayer_animation_finished(anim_001):
	pass # replace with function body


func _on_AnimationPlayer_animation_started(anim_001):
	pass
