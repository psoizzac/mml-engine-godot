extends KinematicBody
var DOWN = -1
var gravity = Vector3(0,DOWN,0)*12
var accel = 0
export var speed = 125
export var speedrate = 10
export var jump_speed = 9
var spin = 0.05
var velocity = Vector3(0,DOWN,0)
var jump = false
var state = "jump"
var rot_x = 0
var rot_y = 0
var rot_z = 0
var floor_normal = Vector3(0,-1,0)
var slope_min_velocity = 0
var max_bounces = 1
var floor_max_angle = deg2rad(45)
var timer = 0
var up = 0
var down = 0
var left = 0
var right = 0
var npc_jump = 0
var turn = 0
var script_c
var wait = 0



func _ready():
	#Called when the scene starts.
	velocity.y = -4
	
	pass

func _process(delta):
	#AI Controls
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	
	if wait == 1:
		up = 0
		down = 0
		left = 0
		right = 0
	
	if Input.is_key_pressed(KEY_Y):
		state = "stand"
		up = 0
		down = 0
		left = 0
		right = 0
	timer +=1
	if timer > 120 and wait == 0:
		down = randi() % 2
		up = randi() % 2
		left = randi() % 2
		right = randi() % 2
		turn = randi() % 12
		timer = 0
		pass
	pass
	
	if left == 1:
		right = 0
		up = 0
		down = 0
	
	if right == 1:
		left = 0
		up = 0
		down = 0
	
	if up == 1:
		right = 0
		left = 0
		down = 0
	
	if down == 1:
		right = 0
		up = 0
		left = 0
	
	if state == "run":
		if not $NPC_Root/AnimationPlayer.current_animation == "anim_000":
			$NPC_Root/AnimationPlayer.play("anim_000")
			$NPC_Root/AnimationPlayer.set_speed_scale(1)
	
	if state == "stand":
		if not $NPC_Root/AnimationPlayer.current_animation == "anim_002":
			$NPC_Root/AnimationPlayer.play("anim_002")
			$NPC_Root/AnimationPlayer.set_speed_scale(0.8)

func _physics_process(delta):
	get_input()
#	velocity = move_and_slide_with_snap(velocity,Vector3(0,-1,0))
	velocity = move_and_slide_with_snap(velocity, Vector3(0,-1,0), Vector3(0,-1,0))
	if not is_on_ceiling() and not state == "stand" :
		velocity += gravity * delta
	if state == "stand":
		velocity.y = 0.00

	if is_on_ceiling():
		if state == "jump":
			velocity.y = -1
			state = "stand"
			
	
	if not is_on_ceiling():
		if state == "walk" or state == "run" and velocity.y < -3:
			state = "jump"
	

	if jump and not state == "jump" and not state == "hurt" and not state == "knockback":
		velocity.y = jump_speed
		state = "jump"
	if accel > speed:
		accel = speed

func get_input():
	var vy = velocity.y
	velocity = Vector3(0,0,0)
	
	if up == 1 and not left == 1 and not right == 1:
		velocity += -transform.basis.z * accel
		accel+=speedrate
		if not state == "jump":
			state = "run"
			if rot_y > 180:
				rot_y-=10
			if rot_y < 180:
				rot_y +=10
			$NPC_Root.set_rotation_degrees(Vector3(0,rot_y,0))
	
	if down == 1 and not left == 1 and not right == 1:
		velocity += transform.basis.z * accel
		velocity.y+=1
		accel+=speedrate
		if not state == "jump":
			state = "run"
			if rot_y > 269:
				rot_y +=10
			if rot_y < 181 and rot_y > 0:
				rot_y -=10
			if rot_y > 359:
				rot_y = 0
			$NPC_Root.set_rotation_degrees(Vector3(0,rot_y,0))
	
	if right == 1 and not up == 1 and not down == 1:
		velocity -= -transform.basis.x * accel
		accel+=speedrate
		if not state == "jump":
			state = "run"
			if not Input.is_action_pressed("move_forward") and not Input.is_action_pressed("move_back"):
				if rot_y > 359:
					rot_y = 0
				
				if rot_y > 90:
					rot_y-=10
				
				if rot_y < 90:
					rot_y+=10
			$NPC_Root.set_rotation_degrees(Vector3(0,rot_y,0))
	
	if left == 1 and not up == 1 and not down == 1:
		velocity += -transform.basis.x * accel
		accel+=speedrate
		if not state == "jump":
			state = "run"
			if not up == 1 and not down == 1:
				
				if rot_y > 359:
					rot_y = 0
				if rot_y < 270:
					rot_y +=10
				if rot_y < 1:
					rot_y -=20
				if rot_y < 89:
					rot_y = 270
				
			$NPC_Root.set_rotation_degrees(Vector3(0,rot_y,0))
	
	if right == 0 and left == 0 and down == 0 and up == 0:
			state = "stand"
		
	if turn == 1: #Turn Left
		rotate_y(spin/5)
		
		pass
	if turn == 2: #Turn Right
		rotate_y(-spin/5)
		pass
		
	if turn == 2: #Turn Right
		rotate_y(-spin/5)
		pass
	
	velocity.y = vy
	jump = false
	if npc_jump == 1:
		jump = true
		npc_jump = 0

#func _unhandled_input(event):
#	if event is InputEventMouseMotion:
#		rotate_y(-lerp(0, spin, event.relative.x/10))
#	pass







func _on_Talk_Area_area_entered(area):
	wait = 1
	$"/root/Globals".npc_name = get_name()
	$"/root/Globals".near_npc = 1

func _on_Talk_Area_area_exited(area):
	wait = 0
	$"/root/Globals".npc_name = "null"
	$"/root/Globals".near_npc = 0
	pass # Replace with function body.
