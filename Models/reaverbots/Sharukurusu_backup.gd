extends KinematicBody

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export var speed = 50
var accel = 0.00
var speedrate = 50
var direction = Vector3()
var gravity = -20
var velocity = Vector3()
var state = "null"
var rot_x = 0
var rot_y = 0
var rot_z = 0
var character = 0
var move_rot_y = 1
var left = 0
var right = 0
var up = 0
var down = 0
var jump = 0
var timer = 0




func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	$Sharukurusu_Root/AnimationPlayer.play_backwards("anim_000")
	pass

func _process(delta):
	#AI Controls
	timer +=1
	if timer > 120:
		down = randi() % 2
		up = randi() % 2
		left = randi() % 2
		right = randi() % 2
		timer = 0
		pass
	pass


func _physics_process(delta):
	move_rot_y +=0.01

	if up == 0 and down == 0 and left == 0 and right == 0:
		accel = 0.00
		state = "stand"
		$Sharukurusu_Root/AnimationPlayer.play_backwards("anim_000")
		pass


	if accel > speed:
		accel = speed

	#Controls and Animations
	direction = Vector3(0,0,0)

	if left == 1 and up == 0 and down == 0:
		direction.x -=1+accel
		accel +=speedrate
		if state != "jump":
			state = "walk"
		rot_y = 270
		$Sharukurusu_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Sharukurusu_Root/AnimationPlayer.current_animation == "anim_001" and state == "walk":
			$Sharukurusu_Root/AnimationPlayer.play("anim_001")

	if right == 1 and up == 0 and down == 0:
		direction.x +=1+accel
		accel +=speedrate
		if state != "jump":
			state = "walk"
		rot_y = 90
		$Sharukurusu_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Sharukurusu_Root/AnimationPlayer.current_animation == "anim_001" and state == "walk":
			$Sharukurusu_Root/AnimationPlayer.play("anim_001")

	if up == 1 and right == 0 and left == 0:
		direction.z -=1+accel
		accel +=speedrate
		if state != "jump":
			state = "walk"
		rot_y = 180
		$Sharukurusu_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Sharukurusu_Root/AnimationPlayer.current_animation == "anim_001" and state == "walk":
			$Sharukurusu_Root/AnimationPlayer.play("anim_001")

	if down == 1 and right == 0 and left == 0:
		direction.z +=1+accel
		accel +=speedrate
		if state != "jump":
			state = "walk"
		rot_y = 360
		$Sharukurusu_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Sharukurusu_Root/AnimationPlayer.current_animation == "anim_001" and state == "walk":
			$Sharukurusu_Root/AnimationPlayer.play("anim_001")


	#Movement and gravity.
	direction = direction.normalized()
	direction = direction * accel * delta

	if velocity.y > 2:
		gravity = -30
	else:
		gravity = -20

	velocity.y += gravity * delta
	velocity.x = direction.x
	velocity.z = direction.z

	if state == "stand":
		if not $Sharukurusu_Root/AnimationPlayer.current_animation == "anim_000" and state == "stand":
			$Sharukurusu_Root/AnimationPlayer.play_backwards("anim_000")


	velocity = move_and_slide(velocity,Vector3(0,1,0))

	#Collision detection
	if is_on_floor() and jump == 1:
		state = "jump"
		if not $Sharukurusu_Root/AnimationPlayer.current_animation == "anim_005":
			$Sharukurusu_Root/AnimationPlayer.play("anim_005")
		velocity.y = 9

	if velocity.y != 0 and $Sharukurusu_Root/AnimationPlayer.current_animation == "anim_005":
			$Sharukurusu_Root/AnimationPlayer.play("anim_006")
			state = "jump"
			pass

	if not is_on_floor():
		state = "jump"

	if state == "jump":
		if not $Sharukurusu_Root/AnimationPlayer.current_animation == "anim_006" and velocity.y >= 4 or velocity.y <= -4:
			$Sharukurusu_Root/AnimationPlayer.play("anim_006")

	if is_on_floor():
		if state != "walk":
			state = "stand"
			$Sharukurusu_Root/AnimationPlayer.play_backwards("anim_000")

	#AI Controls


