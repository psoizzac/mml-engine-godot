extends Area
var velocity = Vector3()
var speed = 0
var def_yscale = 4
var def_xscale = 4
var def_zscale = 4
var def_alpha = 25
var alpha = def_alpha
var size

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	scale.x = 0.005
	scale.y = 0.005
	scale.z = 0.005
	print("Explosion Location = ",transform)
	$explode_sfx.play()
	pass # Replace with function body.



func start(xform):
	global_transform = xform
	scale.x = 0.005
	scale.y = 0.005
	scale.z = 0.005

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _physics_process(delta):
#	print(alpha)
	if scale.x < def_xscale:
		scale.x+=0.05
		if scale.x > 0.25:
			scale.x+=0.1
	
	if scale.y < def_yscale:
		scale.y += 0.05
		if scale.y > 0.25:
			scale.y+=0.1
	
	if scale.z < def_zscale:
		alpha-=0.5
		scale.z +=0.05
		if scale.z > 0.25:
			scale.z+=0.1
	if scale.x >= def_xscale and scale.y >= def_yscale and scale.z >= def_zscale:
			queue_free()
	if alpha < 0.10:
		alpha = 0.00
	$MeshInstance.get_surface_material(0).albedo_color = Color(200,90,0,alpha)
	pass
