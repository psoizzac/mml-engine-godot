extends Node2D
#Main Story flags.
var buster_shot = load("res://buster_shot.tscn")
var buster_inst = buster_shot.instance()
var default_script = load("res://gui/npc_dialogue/message_box.tscn")
var current_scene
var npc_name = "null"
var Player
var player_pos = Vector3(0,0,0)
var player_rot = Vector3(0,0,0)
var rot_y = 0
var cam_direction
var wait = 0
var energy_timer = 1
var reload_timer = 1
var message = "null"
var near_npc = 0
var area = "null"
var player_wait = 0
var msg_box_x_default = 260
var msg_box_y_default = 450
var msg_box_x = 260
var msg_box_y = 450
var player_x
var player_y
var player_z
var locked #If you're standing near a locked door.
var autoplay = 0
var next_message = 0
var jump_springs = 1
var defeated_enemies = 0
var tutorial = 0 #Turn tutorials on or off in game.
var megaman_animation = "anim_000"
var debug_mode = 0
var joke = 0
var helmet = 0

#Declare Main Story Flags below.
var current_game = "Legends1"
var scn_flag = 0
var sub_scn_flag = 0
var demo_scn_flag = 0
var fan_content_mode = 0 #Whether to load fan made content or mods. Currently only affects fan content 
#such as Chiz's Kattelox Times newspaper.

#Declare sidequest Flags below.

#Legends 1 Sidequests
var Servbot_Robbery = 0 #Bank robbery sidequest
var Bomb_threat = 0 #Bomb threat sidequest
var Lost_bag = 0 #Lost bag sidequest.
var needs_some_talent = 0 #Painter lady sidequest.

#Five Island Sidequests


#Destructible Walls if set to 1 the wall is destroyed.
var wall1 = 0
var wall2 = 0
var wall3 = 0
var wall4 = 0
var wall5 = 0
var wall6 = 0
var wall7 = 0
var wall8 = 0
var wall9 = 0

#Money
var zenny = 1000

#Global Interactions
var soda = 0
var question = 0

#Upgrades
var maxhp = 50
var hp = 50
var current_hp = maxhp
var max_wpn_energy = 100
var wpn_energy = 100
var has_jet_skates = 1

#Buster Stats
var power = 1
var ranged = 4*0.25
var rapid = 6
var max_energy = 3
var energy = max_energy
var rapid_timer = 1
var debug_timer = 1

#HP GUI
var pc1 = 0
var pc2 = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pc1 = hp
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.

func _process(delta):
	#HP GUI
	$hp_bar/hp_progress.max_value = maxhp
	$hp_bar/hp_progress.value = current_hp
	if current_hp < 1:
		$hp_bar.frame = 1
	if current_hp > 0:
		$hp_bar.frame = 0
	
	
	if debug_mode == 1:
		if debug_timer < 0.01:
			if soda > 0:
				print("Soda == ",soda)
				print("Question == ",question)
			debug_timer = 1.00
	
	
		
	if not Input.is_action_pressed("attack"):
		if reload_timer > 0:
			reload_timer -=5*delta
			if reload_timer < 0.01:
				energy = max_energy
				rapid_timer = 0.01
				reload_timer = 1
	
#	if Input.is_action_just_pressed("attack"):
#		if energy == max_energy:
#			rapid_timer = 0.00
	
	if Input.is_action_pressed("attack"):
		rapid_timer -=1*delta
		
		if rapid_timer < 0.01:
			rapid_timer = 1-rapid*0.1
	
#	if energy < 1:
#		energy_timer -=1*delta
#		if energy_timer < 0.01:
#			energy = max_energy
#			energy_timer = 1
	
	pass
